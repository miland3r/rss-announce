#!/bin/bash

# ----------------------------------------------------------------------- #
#                                                                         #
#   Copyright 2014, Ulrich-Xander van Brakel                              #
#                                                                         #
#   This program is free software: you can redistribute it and/or modify  #
#   it under the terms of the GNU General Public License as published by  #
#   the Free Software Foundation, either version 3 of the License, or     #
#   (at your option) any later version.                                   #
#                                                                         #
#   This program is distributed in the hope that it will be useful,       #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#   GNU General Public License at <http://www.gnu.org/licenses/> for      #
#   more details.                                                         #
#                                                                         #
#                                                                         #
#        Title: RSS Announce                                              #
#        Author: Ulrich-Xander van Brakel                                 #
#        Email: rot13(<hyevpu.inaoenxry@tznvy.pbz>)                       #
#                                                                         #
#        File: rss_announce.sh                                            #
#        Created: 07 April, 2015                                          #
#                                                                         #
#        Purpose: Announce new RSS messages vocally.                      #
#                                                                         #
# ----------------------------------------------------------------------- #


# Default Properties
# =====================
readonly SNAME=`echo ${0} | sed s"%^./%%"`
readonly VERSION="1.11"

readonly DEFAULT_ENGINE="google-tts"
readonly DEFAULT_LOCALE="en-US"

# " Please take note that '<_lang>' is a placeholder only and will be replaced by \$LANG later in the script."

readonly GOOGLE_TTS_URL="http://translate.google.com/translate_tts?tl=<_lang>&q=" 

# =====================

verbose() {
  readonly VERBOSE="true"
}

function rot13 {
   echo "$@" | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
}

version() {
    echo -e "${SNAME} ${VERSION}\n\n"
    echo "Copyright (C) 2011 Free Software Foundation, Inc."
    echo "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>."
    echo "This is free software: you are free to change and redistribute it."
    echo -e "There is NO WARRANTY, to the extent permitted by law.\n\n"

    echo -n "Written by Ulrich-Xander van Brakel "
    rot13 "<hyevpu.inaoenxry@tznvy.pbz>"

    exit 0
}

usage() { 
    echo -e "\nUsage: ${SNAME} [OPTION]... [URL]..."
    echo -e "List new entries from the specified RSS url using rsstail and plays them back using the specified text to sound engine (google-tts by default) and mplayer.\n"

    echo "  -V       Prints version information then exits."
    echo "  -u       The RSS feed URL to announce."
    echo "  -l       The locale to be used by the text to sound engine (en-US by default)."
    echo "  -e       The text to sound engine to be used for converting the text."
    echo "           Currently supporting google-tts and pico2wave (google-tts by default)."
    echo "  -o       Optional arguments to pass to rsstail.  Please see rsstail -h for details."
    echo -e "  -v       This enables verbose output.\n"

    echo -n "Report ${SNAME} bugs to "
    rot13 "<hyevpu.inaoenxry@tznvy.pbz>"

    exit 1 
}

# ====================

URL_PARAM= LOCALE_PARAM= ENGINE_PARAM= RSS_ARG_PARAM=

while getopts :Vvu:l:e:o: OPT; do
  case ${OPT} in
  V)
      version
      ;;
  v)
      verbose
      ;;
  u)
      URL_PARAM="${OPTARG}"
      ;;
  l)
      LOCALE_PARAM="${OPTARG}"
      ;;
  e)
      ENGINE_PARAM="${OPTARG}"
      ;;
  o)
      RSST_ARG_PARAM="${OPTARG}"
      ;;
  *)
      usage
      ;; 
  esac
done

shift $((OPTIND - 1))

if [ ! -z "${URL_PARAM}" ]; then
    URL=${URL_PARAM}
else
    usage
fi

if [ ! -z "${LOCALE_PARAM}" ]; then
    LOCALE=${LOCALE_PARAM}
    LANG=`echo ${LOCALE} | cut -d'-' -f 1`
else
    LOCALE=${DEFAULT_LOCALE}
    LANG=`echo ${LOCALE} | cut -d'-' -f 1`
fi

if [ ! -z "${ENGINE_PARAM}" ]; then
    ENGINE=${ENGINE_PARAM}
else
    ENGINE=${DEFAULT_ENGINE}
fi

if [ ! -z "RSST_ARG_PARAM" ]; then
    RSST_ARGS="RSST_ARG_PARAM"
fi

# ====================

which rsstail &>/dev/null || {
    echo "error: rsstail not installed"
    exit
}

which mplayer &>/dev/null || {
    echo "error: mplayer not installed"
    exit
}
if [ ${ENGINE} = "pico2wave" ]; then
    which pico2wave &>/dev/null || {
        echo "error: pico2wave not installed"
        exit
    }
fi

if [ ${ENGINE} = "google-tts" ]; then
    which mplayer &>/dev/null || {
        echo "error: wget not installed"
        exit
    }
fi

# ====================

if [ ! -z ${VERBOSE} ]; then
    RSST_VARG="-vv"
fi

rsstail "${RSST_VARG}" "${RSST_ARGS}" -i 15 -u "${URL}" -n 0 -P | while read FEED

do 
    echo "${FEED}"

    if [ ${ENGINE} = "pico2wave" ]; then

        TEMP_WAV=`tempfile -p rssa_ -s .wav`

        pico2wave -l=${LOCALE} -w=${TEMP_WAV} "${FEED}"
        if [ ! -z ${VERBOSE} ]; then
            mplayer --noconsolecontrols -v ${TEMP_WAV}
            echo ""
        else
            mplayer --noconsolecontrols ${TEMP_WAV} &>/dev/null
        fi

        rm ${TEMP_WAV}

    elif [ ${ENGINE} = "google-tts" ]; then 

        TEMP_MPG=`tempfile -p rssa_ -s .mpg`
 
        TTS_URL=`echo ${GOOGLE_TTS_URL} | sed -e 's/<_lang>/'"${LANG}"'/'`
       
        if [ ! -z ${VERBOSE} ]; then
            echo -e "\nTTS Request URL: ${TTS_URL}${FEED}"
        fi

        wget -q -U Mozilla -O ${TEMP_MPG} "${TTS_URL}""${FEED}"

        if [ ! -z ${VERBOSE} ]; then
            mplayer --noconsolecontrols ${TEMP_MPG}
            echo ""
        else
            mplayer --noconsolecontrols ${TEMP_MPG} &>/dev/null
        fi

        rm ${TEMP_MPG}
    else
        echo -n "Invalid or not supported engine specified.\n"
        usage
    fi
done

#eof
